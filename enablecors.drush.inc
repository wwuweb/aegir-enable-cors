<?php

/**
 * @file
 * Enable Cross-site origin request sharing.
 */

/**
 * Implements hook_provision_apache_vhost_config().
 */
function enablecors_provision_apache_vhost_config($uri, $data) {
  if (preg_match('@library(dev|test|prod|)?\dx\d+\.wwu\.edu$@', $uri) === 1
    || preg_match('@^westerntoday(dev|test|prod|)?\dx\d+\.wwu\.edu$@', $uri) === 1
    || preg_match('@^fairhaven(dev|test|prod|)?\dx\d+\.wwu\.edu$@', $uri) === 1) {
    $headers[] = '';

    $headers[] = '<IfModule mod_headers.c>';
    $headers[] = '  Header set Access-Control-Allow-Origin "*"';
    $headers[] = '</IfModule>';

    $headers[] = '';

    return $headers;
  }
}